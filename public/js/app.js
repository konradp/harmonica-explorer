let harp = null;
let notes = []; // they start with G: G, G#, A, ...
let is_mobile = false;
const maj_intervals = [ 0, 2, 4, 5, 7, 9, 11 ];
const min_intervals = [ 0, 2, 3, 5, 7, 8, 10 ]; // natural
window.addEventListener('resize', checkMobile);

window.onload = function() {
  harp = new Harmonica('canvas');
  notes = ScaleShiftRight(harp.chromatic, 5);

  // Populate settings
  const input_harp = _getId('harp');
  const input_scale = _getId('scale');
  for (note of notes) {
    // scale: harp
    let opt = _new('option');
    opt.innerHTML = note;
    if (note == 'C') {
      opt.setAttribute('selected', true);
    }
    input_harp.addEventListener('change', onUpdateHarp);
    input_harp.appendChild(opt);
    // scale: highlighted
    let opt_hi = _new('option');
    opt_hi.innerHTML = note;
    if (note == 'C') {
      opt_hi.setAttribute('selected', true);
    }
    input_scale.addEventListener('change', onUpdateScale);
    input_scale.appendChild(opt_hi);
  }

  // Update
  checkMobile();
  onUpdateHarp();
  onUpdateScale();
}


function checkMobile() {
  let is_mobile_before = is_mobile;
  if (navigator.userAgent.includes('Mobile')
    || 'ontouchstart' in document.documentElement
  ) {
    // Is mobile
    is_mobile = true;
    console.log('is mobile'); // DEBUG
  } else {
    is_mobile = false;
    console.log('not mobile'); // DEBUG
  }
  if (is_mobile != is_mobile_before) {
    // Update styles
    console.log('update'); // DEBUg
    setStyles();
  }
}


function HighlightNotes() {
  // get harp notes
  // get scale notes
  const h_notes = harp.GetHarpNotes();
  const scale = _getId('scale').value;
  const scale_notes = GetScaleNotes(scale, true);
  let highlight = {
    blow: [],
    draw: [],
  };
  for (let i=0; i<10; i++) {
    if (scale_notes.includes(h_notes.blow[i])) {
      highlight.blow.push(true);
    } else {
      highlight.blow.push(false);
    }
    if (scale_notes.includes(h_notes.draw[i])) {
      highlight.draw.push(true);
    } else {
      highlight.draw.push(false);
    }
  }
  harp.SetColors(highlight);
}

function onUpdateHarp() {
  const tuning = _getId('harp').value;
  harp.SetTuning(tuning);
  HighlightNotes();
}

function onUpdateScale() {
  const scale_maj = _getId('scale').value;

  // Scale names
  const dom_scale_maj = _getId('scale-maj');
  dom_scale_maj.innerHTML = scale_maj + ' maj';
  const dom_scale_min = _getId('scale-min');
  // Relative minor starts on the 6th degree of the major scale
  let scale_min = GetScaleNotes(scale_maj, true)[5];
  dom_scale_min.innerHTML = scale_min + ' min';

  // Scale notes
  const scale_notes_maj = _getId('scale-notes-maj');
  scale_notes_maj.innerHTML = GetScaleNotes(scale_maj, true);
  const scale_notes_min = _getId('scale-notes-min');
  scale_notes_min.innerHTML = GetScaleNotes(scale_min, false);

  // Scale suggestions
  // TODO
  const one_maj = _getId('1st-maj');
  one_maj.innerHTML = scale_maj + ' maj';
  const one_min = _getId('1st-min');
  one_min.innerHTML = scale_min + ' min';
  HighlightNotes();
}


function GetScaleNotes(scale, is_maj) {
  const note = notes.indexOf(scale);
  let scale_notes = [];
  let intervals = (is_maj)? maj_intervals : min_intervals;
  for (interval of intervals) {
    let i = (note + interval)%notes.length;
    scale_notes.push(notes[i]);
  }
  return scale_notes;
}

function ScaleShiftRight(array, count) {
  // Move last element to first
  let arr = [...array];
  for (let i = 1; i <= count; i++) {
    let popped = arr.pop();
    arr.unshift(popped);
  }
  return arr
}


function setStyles() {
  const body = document.getElementById('body');
  if (is_mobile) {
    body.style['font-size'] = '4em';
    harp.canvas.style['width'] = '100%';
  } else {
    body.style['font-size'] = '1.5em';
    harp.canvas.style['width'] = '50%';
  }
}


// Helpers
function _new(tag) {
  return document.createElement(tag);
}
function _getId(id) {
  return document.getElementById(id);
}
