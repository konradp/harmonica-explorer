class Harmonica {
  constructor(canvas_id) {
    this.canvas = _getId(canvas_id);
    this.c = this.canvas.getContext('2d');

    // settings
    this.colors = {
      blow: [], // all false
      draw: [], // all false
    };
    this.tuning = 'C';

    // data
    this.chromatic = [ 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B' ];
    this.harp_offsets = {
      // C E G   C  E  G   C  E  G   C
      // D G B   D  F  A   B  D  F   A
      blow: [ 0, 4, 7,  12, 16, 19,  24, 28, 31,  36],
      draw: [ 2, 7, 11, 14, 17, 21,  23, 26, 29,  33],
    }
    this.tunings = {
      'G':  -5,
      'G#': -4,
      'A':  -3,
      'A#': -2,
      'B':  -1,
      'C':   0,
      'C#':  1,
      'D':   2,
      'D#':  3,
      'E':   4,
      'F':   5,
      'F#':  6,
      'G':   7,
    }
    for (let i = 1; i<=10; i++) {
      // Init colors to no colors
      this.colors.blow.push(false);
      this.colors.draw.push(false);
    }

    // main
    this.Redraw();
  }


  DrawHole(hole, hole_y, label) {
    // hole: 1, 2, 3, ...
    // hole_y: ..., -1, 0, 1, ... (positive is up)
    const c = this.c;
    label = label.toString();
    // Draw box
    const x = this.margin + (hole-1)*this.hole_width;
    const y = this.center_y - this.hole_height/2 - hole_y*this.hole_height;
    const w = this.hole_width;
    const h = this.hole_height;
    c.strokeRect(x, y, w, h);

    // colors
    if (hole_y != 0) {
      let draw_or_blow = (hole_y>0)? 'blow' : 'draw';
      if (this.colors[draw_or_blow][hole-1]) {
        c.fillStyle = 'lightgreen'; // TODO: Move
        c.fillRect(x, y, w, h);
        c.fillStyle = 'black';
        c.strokeRect(x, y, w, h);
      }
    }

    // TODO: The text positioning is guessed
    //       It should be done based on text width and height
    //       and box width and height
    let text_offset_x = (label.length == '1')? this.text_offset_x : this.text_offset_x - this.hole_width/8;
    c.fillText(label,
      x + text_offset_x,
      y + this.hole_height/2 + this.hole_height/5,
    );
  }


  GetHarpNotes() {
    // return:
    // {
    //   blow: [ C E G   C  E  G   C  E  G   C ],
    //   draw: [ D G B   D  F  A   B  D  F   A ],
    // }
    let ret = {
      blow: [],
      draw: [],
    };
    for (let i = 1; i <= 10; i++) {
      ret.blow.push(this.HoleToNote(i, true));
      ret.draw.push(this.HoleToNote(i, false));
    }
    return ret;
  }


  SetColors(colors) {
    // colors = {
    //   blow: [ true, false, ..., true ],
    //   draw: [ ... ],
    // }
    this.colors = colors;
    this.Redraw();
  }


  SetTuning(tuning) {
    this.tuning = tuning;
    this.Redraw();
  }


  HoleToNote(number, is_blow) {
    // number: 0 to 10
    // is_blow: true/false
    const tuning_offset =  this.tunings[this.tuning]+12; // TODO: HARDCODED
    let note = this.harp_offsets.draw[number-1];
    if (is_blow) {
      note = this.harp_offsets.blow[number-1];
    }
    note = note + tuning_offset;
    note = note%this.chromatic.length;
    return this.chromatic[note];
  }

  Redraw() {
    const c = this.c;

    this.canvas.width = window.innerWidth;
    this.canvas.height = 3*window.innerWidth/9;
    this.margin = 0;
    this.width = this.canvas.width;
    //this.width = window.innerWidth;
    this.height = this.canvas.height;
    this.is_mobile = false;
    this.center_y = this.height/2;
    this.harp_height = this.height/3;
    this.harp_width = this.width - 2*this.margin;
    this.harp_top = this.height/2 - this.harp_height/2;
    this.hole_width = this.harp_width/10;
    //this.hole_height = this.harp_height/2;
    this.hole_height = this.hole_width;
    this.text_offset_x = this.hole_width/3;
    this.text_offset_y = this.hole_height/3;
    this.c.font = this.hole_width/2 + 'px sans';

    c.clearRect(0, 0, this.width, this.height);
    for (let i = 1; i <= 10; i++) {
      this.DrawHole(i, 0, i);
      this.DrawHole(i, 1, this.HoleToNote(i, true));
      this.DrawHole(i, -1, this.HoleToNote(i, false));
    }
  }

}; // class Harmonica
