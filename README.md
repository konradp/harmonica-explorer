# harmonica explorer
https://konradp.gitlab.io/harmonica-explorer

Highlight which notes on a harmonica correspond to which scale.  
**Note:** Adjustable harmonica tonation.

![harmonica-explorer](harmonica-explorer.png)

## Run
```
npm install
npm start
```
